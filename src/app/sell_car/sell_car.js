import React, {Component} from 'react';
import Header from '../components/header/header';
import jwt from 'jsonwebtoken';
import { Link } from 'react-router-dom';

class Sell_car extends Component {
    constructor(props) {
        super(props);

        this.cost_ref = React.createRef();
        this.description_ref = React.createRef();
        this.image_ref = React.createRef();
        this.ref_preloader = React.createRef();
    }

    render() {
        return [
            <div className="off-canvas-menu">
                <Header/>
                <div className="wrapper" style={{marginTop: '100px'}}>
                    <div className="main">
                        <div className="section">
                            <div className="container">
                                <h3>Продажа автомобиля</h3>
                                <form>
                                    <div className="row">
                                        <div className="col-md-5 col-sm-5">
                                            <h6>Изображение автомобиля</h6>
                                            <div className="fileinput fileinput-new text-center" data-provides="fileinput">
                                                <div className="fileinput-new thumbnail img-no-padding" style={{maxWidth: '370px', maxHeight: '250px'}}>
                                                    <img ref={this.image_ref} alt="..."/>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-7 col-sm-7">
                                            <div className="form-group">
                                                <h6>Цена</h6>
                                                <input ref={this.cost_ref} type="text" className="form-control border-input"/>
                                            </div>
                                            <div className="form-group">
                                                <h6>Описание</h6>
                                                <textarea ref={this.description_ref} className="form-control textarea-limited" rows="13"/>
                                            </div>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>

                                    <div className="row buttons-row">
                                        <div className="col-md-6 col-sm-6">
                                            <Link to={'/personal_area'} style={{color: 'black'}} className="btn btn-outline-danger btn-block btn-round">Назад</Link>
                                        </div>
                                        <div className="col-md-6 col-sm-6">
                                            <Link to={'/personal_area'} onClick={this.sell.bind(this)} style={{color: 'white'}} className="btn btn-primary btn-block btn-round">Продать</Link>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>,
            <div ref={this.ref_preloader} style={{position: 'fixed', left: 0, top: 0, zIndex: 999, width: '100%', height: '100%', overflow: 'visible', background: "#333 url('http://files.mimoymima.com/images/loading.gif') no-repeat center center"}}><div></div></div>
        ]
    }
    componentWillMount(){
        this.getImageCar.bind(this)();
    }

    sell(){

        const id_car = window.location.search.split('id=')[1];

        const body = 'id_car=' + encodeURIComponent(id_car) +
        '&cost=' + encodeURIComponent(this.cost_ref.current.value) +
        '&description=' + encodeURIComponent(this.description_ref.current.value);

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        fetch('http://localhost:5000/sell_car', {
            method: 'POST',
            credentials: 'include',
            headers: headers,
            body: body
        });

        function getCookie(name) {
            const matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    getImageCar(){
        const xhr = new XMLHttpRequest();
        const that = this;

        const id_car = window.location.search.split('id=')[1];
        const user_id = this.getUserId();

        xhr.open('GET', `http://localhost:5000/get_user_cars?id=${user_id}`);

        xhr.onload = function () {
            JSON.parse(this.responseText).map(e => {
                if(e._id === id_car){
                    that.image_ref.current.src = e.image;

                    // Убрать pre-loader
                    that.ref_preloader.current.style.display = 'none';
                }
            })
        };
        xhr.send();
    }

    getUserId(){
        return jwt.decode(getCookie('token'))._id;

        function getCookie(name) {
            const matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }
}

export default Sell_car;