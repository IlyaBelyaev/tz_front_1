import React, { Component } from 'react';
import Header from '../components/header/header';
import jwt from 'jsonwebtoken';
import { Link } from 'react-router-dom';

class Personal_area extends Component {
    constructor(props){
        super(props);

        this.state = {
            cars: []
        };

        this.ref_preloader = React.createRef();
    }
    render() {
        return [
            <div className="off-canvas-menu">
                <Header/>

                <div className="container tim-container" style={{marginTop: '150px'}}>

                    <div className="row">
                        <div className="col-md-12">
                            <h4>Ваши машины</h4>
                            <Link to='/add_car' style={{color: 'white'}} className="btn btn-success btn-round">+ Добавить</Link>
                        </div>
                        <div className="col-md-8 ml-auto mr-auto">
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th className="text-center">#</th>
                                        <th>Название</th>
                                        <th>Статус</th>
                                        <th className="text-right"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.cars.map((e,i) => {
                                        return (
                                        <tr key={e._id}>
                                            <td className="text-center">{i+1}</td>
                                            <td>{e.car_name}</td>
                                            <td>{e.selling ? 'В продаже' : 'В гараже'}</td>
                                            <td className="td-actions text-right">
                                                <Link to={'/sell_car?id='+e._id} className="btn btn-success btn-round">Продать</Link>
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>,
            <div ref={this.ref_preloader} style={{
                position: 'fixed',
                left: 0,
                top: 0,
                zIndex: 999,
                width: '100%',
                height: '100%',
                overflow: 'visible',
                background: "#333 url('http://files.mimoymima.com/images/loading.gif') no-repeat center center"
            }}><div></div></div>
        ]
    }

    componentWillMount(){
        setInterval(this.getUserCars.bind(this), 1000)
    }

    getUserId(){
        return jwt.decode(getCookie('token'))._id;

        function getCookie(name) {
            const matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    getUserCars(){
        const xhr = new XMLHttpRequest();
        const that = this;

        const user_id = this.getUserId();

        xhr.open('GET', `http://localhost:5000/get_user_cars?id=${user_id}`);

        xhr.onload = function () {
            that.setState({cars: JSON.parse(this.responseText)});
            try{
                that.ref_preloader.current.style.display = 'none';
            }catch (err){}
        };
        xhr.send();
    }
}

export default Personal_area;